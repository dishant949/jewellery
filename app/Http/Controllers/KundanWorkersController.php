<?php

namespace App\Http\Controllers;

use App\labour_type;
use App\kundanWorkers;
use App\labourProfile;
use Illuminate\Http\Request;

class KundanWorkersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  $data=labour_type::select("SELECT * FROM `labour_types` WHERE labourType='kundan work karigar'")->get();
        return view('kundanworker.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // request()->validate([
        //        ''  =>'required',
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kundanWorkers  $kundanWorkers
     * @return \Illuminate\Http\Response
     */
    public function show(kundanWorkers $kundanWorkers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kundanWorkers  $kundanWorkers
     * @return \Illuminate\Http\Response
     */
    public function edit(kundanWorkers $kundanWorkers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kundanWorkers  $kundanWorkers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, kundanWorkers $kundanWorkers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kundanWorkers  $kundanWorkers
     * @return \Illuminate\Http\Response
     */
    public function destroy(kundanWorkers $kundanWorkers)
    {
        //
    }
}
