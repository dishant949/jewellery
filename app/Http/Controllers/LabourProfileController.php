<?php

namespace App\Http\Controllers;

use App\labour_type;
use App\labourProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class LabourProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=labourProfile::with('labour_type')->get();
        return view('labourprofile.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $data=labour_type::get();
        return view('labourprofile.create',compact('data'));
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
             'labourType_id' => 'required',
             'name'=>'required',
             'fatherName'=> 'required',
             'mobile'=>'required',
             'email'=>'required',
             'address'=>'required',
             'media'=>'required',

        ]);
        
        $labourProfile= new labourProfile;
        $labourProfile->labourType_id = request('labourType_id');
        $labourProfile->name = request('name');
        $labourProfile->fatherName = request('fatherName');
        $labourProfile->mobile = request('mobile');
        $labourProfile->email = request('email');
        $labourProfile->address= request('address');
        $labourProfile->media = request('media')->hashName();
        request('media')->store('public/labourprofile');
        $labourProfile->save();
        return redirect('labourprofile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\labourProfile  $labourProfile
     * @return \Illuminate\Http\Response
     */
    public function show(labourProfile $labourProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\labourProfile  $labourProfile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    
        
           return view('labourprofile.edit',[
               'labour'=>labour_type::all(),
               'data'=>labourProfile::find(Crypt::decrypt($id)),
           ]);
           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\labourProfile  $labourProfile
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {    
    
        request()->validate([
            'labourType_id' => 'required',
            'name'=>'required',
            'fatherName'=> 'required',
            'mobile'=>'required',
            'email'=>'required',
            'address'=>'required',
             //'media'=>'required', if we don't change media so comment this line 

       ]);
      
        $labourProfile=labourProfile::find(Crypt::decrypt($id));
        $labourProfile->labourType_id = Crypt::decrypt(request('labourType_id'));
        $labourProfile->name = request('name');
        $labourProfile->fatherName = request('fatherName');
        $labourProfile->mobile = request('mobile');
        $labourProfile->email = request('email');
        $labourProfile->address= request('address');
       
         if(request('media')) {
             Storage::delete('public/labourprofile/'.$labourProfile->media);
            $labourProfile->media = request('media')->hashName();
            request('media')->store('public/labourprofile');
         }
        $labourProfile->save();
        return redirect('labourprofile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\labourProfile  $labourProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        labourProfile::find(Crypt::decrypt($id))->delete();
        return  redirect('labourprofile');
    }
}
