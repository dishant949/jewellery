<?php

namespace App\Http\Controllers;

use App\labour_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class LabourTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = labour_type::all();
         return view('labour.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('labour.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         request()->validate([
             'labourType'=>'required',
         ]);
       
         $lab = new labour_type();
            $lab->labourType = request('labourType');
            $lab->save();
            return redirect('labour');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\labour_type  $labour_type
     * @return \Illuminate\Http\Response
     */
    public function show(labour_type $labour_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\labour_type  $labour_type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data = labour_type::findOrFail(Crypt::decrypt($id));
           return view('labour.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\labour_type  $labour_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'labourType'=>'required',
        ]);
      $data = labour_type::findOrFail(Crypt::decrypt($id));
      $data->labourType = request('labourType');
      $data->save();
        return redirect('labour');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\labour_type  $labour_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        labour_type::findOrFail(Crypt::decrypt($id))->delete();
        return redirect('labour');
    }
}
