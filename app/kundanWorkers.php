<?php

namespace App;

use App\labour_type;
use App\labourProfile;
use Illuminate\Database\Eloquent\Model;

class kundanWorkers extends Model
{
    protected $guarded= [];
    // public function labourprofile(){
    //     return $this->belongTo(labourProfile::class,'name');
    // }
    public function labour_type(){
        return $this->belongTo(labour_type::class,'labourType');
    }
}

