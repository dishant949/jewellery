<?php

namespace App;

use App\labour_type;
use Illuminate\Database\Eloquent\Model;

class labourProfile extends Model
{
    protected $fillable=['labourType','name','fatherName','mobile','email','address','media'];
    public function labour_type(){
            return $this->belongsTo(labour_type::class,'labourType_id');
        
    }
}
