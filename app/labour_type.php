<?php

namespace App;

use App\labourProfile;
use Illuminate\Database\Eloquent\Model;

class labour_type extends Model
{
    protected $fillable = ['labourType'];
    public function labourProfile(){
        // return $this->belongsTo('aap\labourProfile','labourType_id');
         return $this->belongTo(labourProfile::class);
    }
}
