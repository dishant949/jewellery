<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabourProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labour_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('labourType_id');
            $table->string('name');
            $table->string('fatherName');
            $table->string('email');
            $table->string('mobile');
            $table->text('address');
            $table->string('media');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labour_profiles');
    }
}
