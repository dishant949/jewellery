<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKundanWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kundan_workers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('labourProfile_id');
            $table->string('item');
            $table->text('desc')->nullable();
            $table->string('metal')->nullable();
            $table->string('nagType')->nullable();
            $table->string('kundan');
            $table->string('totalNag');
            $table->string('labourPerNag');
            $table->string('kundanLabour')->nullable();
            $table->string('media')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kundan_workers');
    }
}
