<script>
    // image show code
    var showFile = function(event) {
  var reader = new FileReader();
  reader.onload = function(){
  var output = document.getElementById('img');
  output.src = reader.result;
  };
  reader.readAsDataURL(event.target.files[0]);
  };
    // end image show code
  
  </script>
  <style>
  
  </style>
  @extends('layouts.app')
  @section('content')
  
  <div class="row justify-content-center">
  <div class="card col-md-8">
      <div class="card-header">Create kundanWork karigar</div>
  
      <div class="card-body">
          <form  action="/kundanworker" method="post" enctype="multipart/form-data">
              @csrf 
              <img class="img-fluid profileimg" style="float: right; height:150px; width:150px; margin-right: 75px;
              border-radius: 10px; " id="img" alt="select image" >
              <div class="col-sm-6 md-8 mb-sm-0">
                 
                  <div class="form-group ">
                      <label for="lt">kundan karigar</label><br>
                      <select class="form-select"aria-label=".form-select" name="labourProfile_id" id="lt">
                          <option value="">Select Karigar name</option>
                          @foreach ($data as $item)
                          <option value="{{$item->id}}">{{$item->name}}</option>
                          @endforeach
                      </select>
                  </div>
                  <div class="form-group ">
                      <label for="nm">Jewellery Item</label>
                      <input type="text" name="item" class="form-control form-control-user" id="nm" placeholder="Enter Jewellery type" >
                  </div>
              </div> 
                  <div class="form-group row">
                    <div class="col-md-6"> 
                      <label for="fn">Description</label>
                      <input type="text" name="desc" class="form-control form-control-user" id="fn" placeholder="Enter Father Name" >
                    </div>
                    <div class="col-md-6">   
                      <label for="fl" class="form-label">Jewellery Photo</label>
                      <input type="file" name="media" class="form-control "  onchange="showFile(event)" id="fl">
                   </div>
                 </div>
                 <div class="form-group row">
                  <div class="col-md-6"> 
                    <label for="bnn">Metal</label>
                    <input type="text" name="metal" class="form-control form-control-user" id="bnn" placeholder="Enter Mobile No. " >
                  </div>
                  <div class="col-md-6">   
                    <label for="el" class="form-label">Nag Type</label>
                    <input type="email" name="nagType" class="form-control " id="el">
                 </div>
               </div>
               <div class="form-group row">
                    <div class="col-md-6">
                        <label for="al" class="form-label">Kundan</label>
                        <input type="text" name="kundan" class="form-control " id="al">
                    </div>
                    <div class="col-md-6">
                        <label for="al" class="form-label">Total Nag</label>
                        <input type="text" name="totalNag" class="form-control " id="al">
                    </div>
               </div>
               <div class="form-group row">
                <div class="col-md-6">
                    <label for="al" class="form-label">Labour Per Nag</label>
                    <input type="text" name="labourPerNag" class="form-control " id="al">
                </div>
                <div class="col-md-6">
                    <label for="al" class="form-label">Kundan Labour</label>
                    <input type="text" name="kundanLabour" class="form-control " id="al">
                </div>
                 </div>
               

             <div class="form-group">
               <button type="submit" class="btn btn-primary btn-user ">Submit</button>
           </div>
              
          </form>
       </div>
  </div>
  </div>
  
  @endsection