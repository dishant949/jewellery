@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
<div class="card col-md-8">
    <div class="card-header">Create Labour Type</div>

    <div class="card-body">
        <form  action="/labour" method="post">
            @csrf
            <div class="col-sm-6 md-8 mb-sm-0">
            <div class="form-group ">
             <input type="text" name="labourType" class="form-control form-control-user" id="exampleFirstName" placeholder="Labour Type" >
             </div>
           <div class="form-group">
             <button type="submit" class="btn btn-primary btn-user ">Submit</button>
         </div>
             </div>
        </form>
     </div>
</div>
</div>
@endsection