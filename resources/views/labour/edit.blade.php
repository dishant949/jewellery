@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
<div class="card col-md-8">
    <div class="card-header">Edit Labour Type</div>

    <div class="card-body">
        <form  action="/labour/{{Crypt::encrypt($data->id)}}" method="post">
            @method('put')
            @csrf
            <div class="col-sm-6 md-8 mb-sm-0">
            <div class="form-group ">
             <input type="text" name="labourType" class="form-control form-control-user" id="exampleFirstName" placeholder="Labour Type" value="{{$data->labourType}}">
             </div>
           <div class="form-group">
             <button type="submit" class="btn btn-primary btn-user ">Update</button>
         </div>
             </div>
        </form>
     </div>
</div>
</div>
@endsection