@extends('layouts.app')
@section('content')
{{-- <div class="card shadow ">
    <div class="card-header ">
        <h6 class=" font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $dt)
                     <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$dt->labourType}}</td>
                        <td>
                            <a href="/labour/{{Crypt::encrypt($dt->id)}}/edit"><i class="far fa-edit"></i></a>
                            <form action="/labour/{{Crypt::encrypt($dt->id)}}" method="post">
                                @method('delete')
                               @csrf
                              
                                <button type="submit" class="btn btn-danger">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                       
                    </tr>
                    @endforeach
                </tbody>
                 <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot> 

            </table>
        </div>
    </div>
</div>

</div> --}}
<style>
   
    
</style>

<div class="container">
    <div class="row" >
        <div class="col-md-10">
            <div class="card  color">
                <div class="card-header ">  
                    <a style="float: right;" href="/labour/create">Add New Karigar</a>
                      <h4>Labour List</h4>
                    
                    <table class="table table-bordered" id="dataTable" width="100%"            cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                    
                            </tr>
                        </thead>
                </div>
                <div class="card-body p-0">
                    <tbody>
                        @foreach ($data as $dt)
                         <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$dt->labourType}}</td>
                            <td class="call">
                                <a href="/labour/{{Crypt::encrypt($dt->id)}}/edit"><i class="far fa-edit"></i>Edit</a>
                                <form action="/labour/{{Crypt::encrypt($dt->id)}}" method="post">
                                    @method('delete')
                                   @csrf
                                  
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                            </td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </div>
                <div class="card-footer">
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </tfoot> 
                
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
 
   
    

@endsection 