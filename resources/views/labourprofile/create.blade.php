<script>
  // image show code
  var showFile = function(event) {
var reader = new FileReader();
reader.onload = function(){
var output = document.getElementById('img');
output.src = reader.result;
};
reader.readAsDataURL(event.target.files[0]);
};
  // end image show code

</script>
<style>

</style>
@extends('layouts.app')
@section('content')

<div class="row justify-content-center">
<div class="card col-md-8">
    <div class="card-header">Create Labour Profile</div>

    <div class="card-body">
        <form  action="/labourprofile" method="post" enctype="multipart/form-data">
            @csrf 
            <img class="img-fluid profileimg" style="float: right; height:150px; width:150px; margin-right: 75px;
            border-radius: 10px; " id="img" alt="select image" >
            <div class="col-sm-6 md-8 mb-sm-0">
               
                <div class="form-group ">
                    <label for="lt">Labour Type</label><br>
                    <select class="form-select"aria-label=".form-select" name="labourType_id" id="lt">
                        <option value="">Select Karigar Type</option>
                        @foreach ($data as $item)
                        <option value="{{$item->id}}">{{$item->labourType}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group ">
                    <label for="nm">Name</label>
                    <input type="text" name="name" class="form-control form-control-user" id="nm" placeholder="Enter Full Name" >
                </div>
            </div> 
                <div class="form-group row">
                  <div class="col-md-6"> 
                    <label for="fn">Father Name</label>
                    <input type="text" name="fatherName" class="form-control form-control-user" id="fn" placeholder="Enter Father Name" >
                  </div>
                  <div class="col-md-6">   
                    <label for="fl" class="form-label">Profile</label>
                    <input type="file" name="media" class="form-control "  onchange="showFile(event)" id="fl">
                 </div>
               </div>
               <div class="form-group row">
                <div class="col-md-6"> 
                  <label for="bnn">mobile</label>
                  <input type="text" name="mobile" class="form-control form-control-user" id="bnn" placeholder="Enter Mobile No. " >
                </div>
                <div class="col-md-6">   
                  <label for="el" class="form-label">Email</label>
                  <input type="email" name="email" class="form-control " id="el">
               </div>
             </div>
             <div class="form-group">
                <label for="al" class="form-label">Address</label>
                <input type="text" name="address" class="form-control " id="al">
             </div>
           <div class="form-group">
             <button type="submit" class="btn btn-primary btn-user ">Submit</button>
         </div>
            
        </form>
     </div>
</div>
</div>

@endsection