@extends('layouts.app')
@section('content')

<div class="row justify-content-center">
<div class="card col-md-8">
    <div class="card-header">Edit Labour Profile</div>

    <div class="card-body">
        <form  action="/labourprofile/{{Crypt::encrypt($data->id)}}" method="post" enctype="multipart/form-data">
            @method('put')
            @csrf 
            <div class="">
            <img class="img-fluid" style="float: right; height:150px; width:150px;" src="{{Storage::Url('public/labourprofile/'.$data->media)}}" alt="image not" >
          </div>
            <div class="col-sm-6 md-8 mb-sm-0">
               
                <div class="form-group ">
                    <label for="lt">Labour Type</label><br>
                    <select class="form-select"aria-label=".form-select" name="labourType_id" id="lt">
                        <option value="">Select Karigar Type</option>
                        @foreach ($labour as $item)
                        <option value="{{Crypt::encrypt($item->id)}}"{{$item->id==$data->labourType_id?'selected':''}}>{{$item->labourType}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group ">
                    <label for="nm">Name</label>
                    <input type="text" name="name" class="form-control form-control-user" id="nm" placeholder="Enter Full Name"  value="{{$data->name}}">
                </div>
            </div> 
                <div class="form-group row">
                  <div class="col-md-6"> 
                    <label for="fn">Father Name</label>
                    <input type="text" name="fatherName" class="form-control form-control-user" id="fn" placeholder="Enter Father Name" value="{{$data->fatherName}}" >
                  </div>
                  <div class="col-md-6">   
                    <label for="fl" class="form-label">Profile</label>
                    <input type="file" name="media" class="form-control " id="fl" value="{{$data->media}}">
                 </div>
               </div>
               <div class="form-group row">
                <div class="col-md-6"> 
                  <label for="bnn">mobile</label>
                  <input type="text" name="mobile" class="form-control form-control-user" id="bnn" placeholder="Enter Mobile No. " value="{{$data->mobile}}" >
                </div>
                <div class="col-md-6">   
                  <label for="el" class="form-label">Email</label>
                  <input type="email" name="email" class="form-control " id="el" value="{{$data->email}}">
               </div>
             </div>
             <div class="form-group">
                <label for="al" class="form-label">Address</label>
                <input type="text" name="address" class="form-control " id="al" value="{{$data->address}}">
             </div>
           <div class="form-group">
             <button type="submit" class="btn btn-primary btn-user ">Submit</button>
         </div>
            
        </form>
     </div>
</div>
</div>
@endsection