@extends('layouts.app')
@section('content')



<div class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="card  color">
                <div class="card-header ">  
                    <a style="float: right;" href="/labourprofile/create">Add New Karigar</a>
                      <h4>Labour Profile List</h4>
                    
                    <table class="table table-bordered"  width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>labourType</th>
                                <th>Name</th>
                                <th>Father Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Profile</th>
                                <th>view</th>
                                <th>Action</th>
                    
                            </tr>
                        </thead>
                </div>
                <div class="card-body p-0">
                    <tbody>
                        @foreach ($data as $lb)
                         <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$lb->labour_type->labourType}}</td>
                            <td>{{$lb->name}}</td>
                            <td>{{$lb->fatherName}}</td>
                            <td>{{$lb->mobile}}</td>
                            <td>{{$lb->email}}</td>
                            <td>{{$lb->address}}</td>
                            <td><img src="{{Storage::Url('public/labourprofile/'.$lb->media)}}" alt="" height="100px" width="100px"></td>
                            <td><a href="#"><i class="fas fa-eye fa-2x"></i></a>WorkingList</td>
                            <td>
                                <a href="/labourprofile/{{Crypt::encrypt($lb->id)}}/edit"><i class="far fa-edit"></i>Edit</a>
                                <form action="/labourprofile/{{Crypt::encrypt($lb->id)}}" method="post">
                                    @method('delete')
                                   @csrf
                                  <button type="submit" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                            </td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </div>
                <div class="card-footer">
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>labourType</th>
                            <th>Name</th>
                            <th>Father Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Profile</th>
                            <th>Action</th>
                        </tr>
                    </tfoot> 
                
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
 
   
    

@endsection 